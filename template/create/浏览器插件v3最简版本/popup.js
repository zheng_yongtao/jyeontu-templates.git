document.getElementById("getText").addEventListener("click", async () => {
  const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript(
    {
      target: { tabId: tab.id },
      func: () => {
        return document.body.innerText;
      },
    },
    (result) => {
      document.getElementById("output").innerText = result[0].result;
    }
  );
});
