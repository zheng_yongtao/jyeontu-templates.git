// 在页面注入后执行
console.log("Content script loaded");

// 示例：修改页面背景色
document.body.style.backgroundColor = "#f0f0f0";

// 监听来自popup的消息
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "getText") {
    const text = document.body.innerText;
    sendResponse({ content: text });
  }
});
